# GRAPHICS

allows PrtScr to print graphics screens. (CGA/EGA/VGA/MCGA, on PostScript, ESC/P Epson 8/24pin and HP PCL printers)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## GRAPHICS.LSM

<table>
<tr><td>title</td><td>GRAPHICS</td></tr>
<tr><td>version</td><td>2008-07-14a</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-04-28</td></tr>
<tr><td>description</td><td>allows PrtScr to print graphics screens. (CGA/EGA/VGA/MCGA, on PostScript, ESC/P Epson 8/24pin and HP PCL printers)</td></tr>
<tr><td>summary</td><td>allows PrtScr to print graphics screens. (CGA/EGA/VGA/MCGA, on PostScript, ESC/P Epson 8/24pin and HP PCL printers)</td></tr>
<tr><td>keywords</td><td>prtscr graphics printer driver dos</td></tr>
<tr><td>author</td><td>Eric Auer &lt;e.auer -at- jpberlin.de&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer &lt;e.auer -at- jpberlin.de&gt;</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/graphics/</td></tr>
<tr><td>original&nbsp;site</td><td>http://ericauer.cosmodata.virtuaserver.com.br/soft/</td></tr>
<tr><td>platforms</td><td>DOS (Microsoft Visual C++ in C mode, Borland C++), FreeDOS, OpenWatcom, Linux (gcc),</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Graphics</td></tr>
</table>
